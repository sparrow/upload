module codeberg.org/sparrow/upload

go 1.21.0

toolchain go1.23.0

require (
	github.com/pelletier/go-toml/v2 v2.2.3
	golang.org/x/crypto v0.30.0
)
