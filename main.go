package main

import (
	"flag"
	"io"
	"log"
	"net/http"
	"net/url"
	"os"
	"path/filepath"
	"strconv"
	"strings"
	"time"
)

var conf Config
var storage Storage
var dataPath string
var uploadPath string

func logRequest(r *http.Request) {
	log.Printf("Method: %s ,URL: %s", r.Method, r.URL)
}

func addKey(w http.ResponseWriter, r *http.Request) {
	path := strings.Split(r.URL.Path, "/")
	name := path[3]
	if name == "" {
		w.WriteHeader(400)
	}
	for i := range storage.Keys {
		if i == name {
			w.WriteHeader(409)
			return
		}
	}

	params := r.URL.Query()
	isAdmin := false
	if params.Has("admin") {
		log.Println("param found")
		testParam, err := strconv.ParseBool(params.Get("admin"))
		log.Println(testParam)
		if err != nil {
			http.Error(w, "admin param is not bool", 400)
		}
		if testParam {
			isAdmin = true
		}
	}
	key, err := storage.addKey(name, isAdmin, "")
	if err != nil {
		http.Error(w, "Could not generate token", 500)
		return
	}
	w.Write([]byte(key))
}

func listKeys(w http.ResponseWriter) {
	responseString := strings.Builder{}
	for i, key := range storage.Keys {
		responseString.WriteString(strconv.Itoa(int(key.Timestamp)) + " ")
		responseString.WriteString(i + " " + strconv.FormatBool(key.IsAdmin))
		responseString.WriteString("\n")
	}
	w.Write([]byte(responseString.String()))
}

func deleteKey(w http.ResponseWriter, r *http.Request) {
	path := strings.Split(r.URL.Path, "/")
	name := path[3]
	if name == "" {
		http.Error(w, "bad request", http.StatusBadRequest)
		return
	}
	for i := range storage.Keys {
		if i == name {
			storage.removeKey(name)
			w.WriteHeader(204)
			return
		}
	}
	http.Error(w, "not found", http.StatusNotFound)
}

func methodSwitch(w http.ResponseWriter, r *http.Request) {
	logRequest(r)
	switch r.Method {
	case "PUT":
		putFile(w, r)
	case "POST":
		uploadFile(w, r)
	case "GET":
		if r.URL.Path == conf.BasePath.string() {
			ServeWebpage(w, r)
			return
		}
		serveFile(w, r)
	case "DELETE":
		deleteFile(w, r)
	}
}

func userAdminSwitch(w http.ResponseWriter, r *http.Request) {
	logRequest(r)
	authPass, user := storage.checkUser(r)
	if authPass && user.IsAdmin {
		switch r.Method {
		case "PUT":
			addKey(w, r)
		case "GET":
			listKeys(w)
		case "DELETE":
			deleteKey(w, r)

		}
	} else {
		http.Error(w, "unauthorized", http.StatusUnauthorized)
		return
	}
}

func listFiles(w http.ResponseWriter, r *http.Request) {
	responseString := strings.Builder{}
	for name, file := range storage.Files {
		u, _ := url.JoinPath(conf.Scheme.string()+"://", r.Host, name)
		responseString.WriteString(name + " " + strconv.Itoa(int(file.Ttl)) + " " + u)
		responseString.WriteString("\n")
	}
	w.Write([]byte(responseString.String()))
}

func adminFileSwitch(w http.ResponseWriter, r *http.Request) {
	logRequest(r)
	authPass, user := storage.checkUser(r)
	if authPass && user.IsAdmin {
		switch r.Method {
		case "GET":
			listFiles(w, r)
		case "DELETE":
			deleteKey(w, r)
		}
	} else {
		w.WriteHeader(401)
		return
	}

}

func putFile(w http.ResponseWriter, r *http.Request) {
	userExists, user := storage.checkUser(r)
	if !userExists {
		w.Header().Add("WWW-Authenticate", "Basic realm='Auth pls'")
		w.WriteHeader(401)
		return
	}

	log.Printf("%+v\n", r.Header)

	ctype, hasCtypeHeader := r.Header["Content-Type"]

	filetype := []string{}
	if hasCtypeHeader {
		filetype = strings.Split(ctype[0], "/")
	}

	log.Println(filetype)

	ext := ""
	if len(filetype) > 1 {
		ext = "." + filetype[1]
	}

	newRand := RandString(4)
	destFileName := newRand + ext
	destPath := filepath.Join(uploadPath, destFileName)

	newfile, err := os.Create(destPath)
	if err != nil {
		http.Error(w, "Oops, an internal error occured!", 500)
		log.Println(err)
		return
	}

	defer newfile.Close()
	writtenBytes, err := io.Copy(newfile, r.Body)

	if err != nil {
		http.Error(w, "Failed to upload file", 500)
		log.Println(err)
		return
	}
	log.Printf("wrote %d bytes", writtenBytes)

	log.Printf("%+v", r.Host)
	u, _ := url.JoinPath(conf.Scheme.string()+"://", r.Host, destFileName)

	w.Write([]byte(u))

	opt := fileOpts{
		FileName:    destFileName,
		KeyName:     user.Name,
		IsPrivate:   false,
		IsSingleUse: false,
	}
	storage.addFile(opt)

}

func uploadFile(w http.ResponseWriter, r *http.Request) {
	userExists, user := storage.checkUser(r)
	if !userExists {
		w.Header().Add("WWW-Authenticate", "Basic realm='Auth pls'")
		http.Error(w, "unauthorized", http.StatusUnauthorized)
		return
	}

	isPrivate := false
	isSingleUse := false

	if r.URL.Query().Has("private") {
		p, err := strconv.ParseBool(r.URL.Query().Get("private"))
		if err != nil {
			http.Error(w, "Could not parse private parameter", http.StatusInternalServerError)
			return
		}
		isPrivate = p
	}

	if r.URL.Query().Has("single") {
		p, err := strconv.ParseBool(r.URL.Query().Get("single"))
		if err != nil {
			http.Error(w, "could not parse single parameter", http.StatusInternalServerError)
			return
		}
		isSingleUse = p
	}

	r.ParseMultipartForm(conf.MaxUploadBytes)
	log.Printf("content length :%d", r.ContentLength)

	if r.Form.Has("private") && r.Form.Get("private") == "true" {
		log.Println("file marked as private")
		isPrivate = true
	}

	if r.Form.Has("single") && r.Form.Get("single") == "true" {
		log.Println("file marked as single use")
		isSingleUse = true
	}

	if r.ContentLength > conf.MaxUploadBytes {
		log.Println("file was too big")
		w.WriteHeader(413)
		return
	}

	file, handler, err := r.FormFile("file")
	if err != nil {
		log.Println("Error Retrieving the File")
		log.Println(err)
		w.WriteHeader(500)
		return
	}
	defer file.Close()

	log.Printf("Uploaded File: %+v\n", handler.Filename)
	log.Printf("File Size: %+v\n", handler.Size)
	log.Printf("MIME Header: %+v\n", handler.Header)

	ext := filepath.Ext(handler.Filename)

	newRand := RandString(4)
	destFileName := newRand + ext
	destPath := filepath.Join(uploadPath, destFileName)

	newfile, err := os.Create(destPath)
	if err != nil {
		http.Error(w, "Oops, an internal error occured!", http.StatusInternalServerError)
		log.Println(err)
		return
	}

	defer newfile.Close()
	writtenBytes, err := io.Copy(newfile, file)

	if err != nil {
		http.Error(w, "Failed to upload file", http.StatusInternalServerError)
		log.Println(err)
		return
	}
	log.Printf("wrote %d bytes", writtenBytes)

	log.Printf("%+v", r.Host)
	u, _ := url.JoinPath(conf.Scheme.string()+"://", r.Host, destFileName)

	w.Write([]byte(u))

	opt := fileOpts{
		FileName:    destFileName,
		KeyName:     user.Name,
		IsPrivate:   isPrivate,
		IsSingleUse: isSingleUse,
	}
	storage.addFile(opt)
}

func ServeWebpage(w http.ResponseWriter, r *http.Request) {
	http.ServeFile(w, r, conf.WebIndex.string())
}

func serveFile(w http.ResponseWriter, r *http.Request) {
	path := strings.Split(r.URL.Path, "/")
	last_path := path[len(path)-1]
	if f, ok := storage.Files[last_path]; ok {
		file := filepath.Join(uploadPath, last_path)
		if f.Private {
			authPass, _ := storage.checkUser(r)
			if !authPass {
				w.Header().Add("WWW-Authenticate", "Basic realm='Auth pls'")
				http.Error(w, "unauthorized", http.StatusUnauthorized)
				return
			}
		}
		http.ServeFile(w, r, file)
		if f.SingleUse {
			opt := fileOpts{
				FileName: last_path,
			}
			storage.removeFile(opt)
		}
	} else {
		http.Error(w, "not found", http.StatusNotFound)
		log.Println("404 " + r.URL.Path)
	}
}

func deleteFile(w http.ResponseWriter, r *http.Request) {
	authPass, user := storage.checkUser(r)
	if authPass && user.IsAdmin {
		path := strings.Split(r.URL.Path, "/")
		last_path := path[len(path)-1]
		if _, ok := storage.Files[last_path]; ok {
			opt := fileOpts{FileName: last_path}
			storage.removeFile(opt)
		} else {
			http.Error(w, "not found", http.StatusNotFound)
			log.Println("404 " + r.URL.Path)
		}
	} else {
		http.Error(w, "unauthorized", http.StatusUnauthorized)
		return
	}
}

func checkUploadDir() {
	dirExists, err := doesFileExist(uploadPath)
	if err != nil {
		log.Fatal(err)
	}
	if !dirExists {
		err := os.Mkdir(uploadPath, 0755)
		if err != nil {
			log.Fatal(err)
		}
	}
}

func main() {

	var err error

	configPath := flag.String("config", "/etc/upload.toml", "config path")
	flag.Parse()

	if configExists, err := doesFileExist(*configPath); err == nil && configExists {
		conf, err = newConfigFromFile(*configPath)
	} else if err != nil {
		log.Fatal(err)
	}

	conf.applyDefaults()
	conf.applyFromEnv()
	conf.logOptions()

	conf.parsedTTL, err = time.ParseDuration(conf.TTL.string())
	if err != nil {
		log.Fatal(err)
	}
	conf.parsedScantime, err = time.ParseDuration(conf.ScanTime.string())
	log.Println("scannning every " + conf.parsedScantime.String())
	if err != nil {
		log.Fatal(err)
	}

	dataPath = filepath.Join(conf.DataPath.string(), "data.json")
	uploadPath = filepath.Join(conf.DataPath.string(), "uploads")

	storage.loadOrInit()
	checkUploadDir()

	ticker := time.NewTicker(conf.parsedScantime)
	go func() {
		for ; true; <-ticker.C {
			storage.scan()
		}
	}()

	userAdminPath, err := url.JoinPath(conf.BasePath.string(), "admin/user/")
	if err != nil {
		log.Fatal(err)
	}

	fileAdminPath, err := url.JoinPath(conf.BasePath.string(), "admin/files/")
	if err != nil {
		log.Fatal(err)
	}

	http.HandleFunc(userAdminPath, userAdminSwitch)
	http.HandleFunc(fileAdminPath, adminFileSwitch)
	http.HandleFunc(conf.BasePath.string(), methodSwitch)

	err = http.ListenAndServe(":"+conf.ListenPort.string(), nil)
	if err != nil {
		log.Fatal(err)
	}
}
