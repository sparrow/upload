package main

import (
	"encoding/json"
	"log"
	"net/http"
	"os"
	"path/filepath"
	"strings"
	"time"

	"golang.org/x/crypto/bcrypt"
)

type Key struct {
	Timestamp int64  `json:"timestamp"`
	Token     []byte `json:"token"`
	IsAdmin   bool   `json:"isadmin"`
	Name      string `json:"-"`
}

type File struct {
	Ttl       int64  `json:"ttl"`
	KeyName   string `json:"keyName"`
	Private   bool   `json:"private"`
	SingleUse bool   `json:"singleUse"`
}

type fileOpts struct {
	FileName    string
	KeyName     string
	IsPrivate   bool
	IsSingleUse bool
}

type Storage struct {
	Files map[string]File `json:"files"`
	Keys  map[string]Key  `json:"keys"`
}

var nullUser = Key{}

func (s *Storage) loadOrInit() {
	dataPathExists, err := doesFileExist(dataPath)
	if err != nil {
		log.Fatal(err)
	}
	if dataPathExists {
		log.Println("Found existing data.json")
		fileData, err := os.ReadFile(dataPath)
		if err != nil {
			log.Fatal("Could not read data file")
		}
		json.Unmarshal(fileData, &s)
	} else {
		log.Println("Could not find data.json, creating")
		s.Files = make(map[string]File)
		s.Keys = make(map[string]Key)
		token, err := s.addKey("admin", true, conf.PresetAdminToken.string())
		if err != nil {
			log.Fatal("Could not generate initial admin token")
		}
		s.sync()
		log.Println("====================")
		log.Println("")
		log.Println("A new key for the user \"admin\" has been created with the token:", token)
		log.Println("")
		log.Println("====================")
	}
}

func (s *Storage) sync() {
	jsonData, err := json.Marshal(s)
	if err != nil {
		log.Fatal(err)
	}
	err = os.WriteFile(dataPath, jsonData, 0644)
	if err != nil {
		log.Fatal(err)
	}
}

func (s *Storage) scan() {
	state_altered := false
	for name, file := range s.Files {
		path := filepath.Join(uploadPath, name)
		fileExists, err := doesFileExist(path)
		if err != nil {
			log.Println(err)
			continue
		}
		if !fileExists {
			log.Println("found file: " + name + " in data but not on disk, cleaning up.")
			delete(s.Files, name)
			state_altered = true
			continue
		}
		ttl := file.Ttl + int64(conf.parsedTTL.Seconds())
		if time.Now().Unix() > ttl {
			opt := fileOpts{FileName: name}
			s.removeFile(opt)
			state_altered = true
			continue
		}
	}
	if state_altered {
		s.sync()
	}
}

func (s *Storage) addFile(opt fileOpts) {
	newFile := File{
		Ttl:       time.Now().Unix(),
		KeyName:   opt.KeyName,
		Private:   opt.IsPrivate,
		SingleUse: opt.IsSingleUse,
	}
	s.Files[opt.FileName] = newFile
	s.sync()
}

func (s *Storage) removeFile(opt fileOpts) {
	path := filepath.Join(uploadPath, opt.FileName)
	log.Println("removing: " + opt.FileName)
	err := os.Remove(path)
	if err != nil {
		log.Println(err)
	} else {
		delete(s.Files, opt.FileName)
	}
	s.sync()
}

func (s *Storage) addKey(name string, isAdmin bool, presetToken string) (string, error) {
	var err error
	newToken := RandString(20)
	if presetToken != "" {
		newToken = presetToken
	}
	password, err := bcrypt.GenerateFromPassword([]byte(newToken), bcrypt.DefaultCost)
	if err != nil {
		return "", err
	}
	var newKey Key
	newKey.Token = password
	newKey.IsAdmin = isAdmin
	newKey.Timestamp = time.Now().Unix()

	s.Keys[name] = newKey
	s.sync()
	return newToken, err

}

func (s *Storage) removeKey(name string) {
	log.Println("removing: " + name)
	for i := range s.Keys {
		if i == name {
			delete(s.Keys, i)
			break
		}
	}
	s.sync()
}

func checkPassword(password string, key Key) bool {
	if err := bcrypt.CompareHashAndPassword(key.Token, []byte(password)); err == nil {
		return true
	}
	return false
}

func (s *Storage) checkUser(r *http.Request) (bool, Key) {
	user, pass, hasBasicAuth := r.BasicAuth()
	authnHeader, hasAuthnHeader := r.Header["Authentication"]
	authzHeader, hasAuthzHeader := r.Header["Authorization"]
	querypass := r.URL.Query().Get("token")
	if querypass != "" {
		log.Println("Begin basic auth test for host:", r.RemoteAddr)
		for i, key := range s.Keys {
			if checkPassword(querypass, key) {
				key.Name = i
				log.Printf("successful auth for %s at %s:", i, r.RemoteAddr)
				return true, key
			}
		}
	}
	if hasBasicAuth {
		log.Println("Begin basic auth test for host:", r.RemoteAddr)
		key, hasUser := s.Keys[user]
		if !hasUser {
			log.Printf("Invalid user %s for host %s", user, r.RemoteAddr)
			return false, nullUser
		} else {
			if checkPassword(pass, s.Keys[user]) {
				key.Name = user
				log.Printf("successful auth for %s at %s:", user, r.RemoteAddr)
				return true, key
			}
		}
	}
	if hasAuthnHeader {
		log.Println("Begin authn header test for host:", r.RemoteAddr)
		password := authnHeader[0]
		for i, key := range s.Keys {
			if checkPassword(password, key) {
				key.Name = i
				log.Printf("successful auth for %s at %s:", i, r.RemoteAddr)
				return true, key
			}
		}
	}
	if hasAuthzHeader {
		log.Println("Begin authz test for host:", r.RemoteAddr)
		hsplit := strings.Split(authzHeader[0], " ")
		if len(hsplit) != 2 {
			log.Println("not enough args for Authorization header from host:", r.RemoteAddr)
			return false, nullUser
		}
		password := hsplit[1]
		scheme := hsplit[0]
		log.Println("Authorization scheme:", scheme)
		for i, key := range s.Keys {
			if checkPassword(password, key) {
				key.Name = i
				log.Printf("successful auth for %s at %s:", i, r.RemoteAddr)
				return true, key
			}
		}
	}
	log.Println("Failed authentication from host:", r.RemoteAddr)
	return false, nullUser
}
