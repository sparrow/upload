package main

import (
	"log"
	"os"
	"strconv"
	"time"

	"github.com/pelletier/go-toml/v2"
)

type Config struct {
	DataPath         configString  `toml:"dataPath"`
	TTL              configString  `toml:"ttl"`
	ScanTime         configString  `toml:"scanTime"`
	parsedScantime   time.Duration `toml:"-"`
	parsedTTL        time.Duration `toml:"-"`
	ListenPort       configString  `toml:"listenPort"`
	BasePath         configString  `toml:"basePath"`
	Scheme           configString  `toml:"scheme"`
	StorageFile      configString  `toml:"-"`
	UploadDir        configString  `toml:"-"`
	WebIndex         configString  `toml:"webIndex"`
	PresetAdminToken configString  `toml:"presetAdminToken"`
	MaxUploadBytes   int64         `toml:"maxUploadBytes"`
}

type configString string

func (v *configString) set(value string) {
	a := configString(value)
	*v = a
}

func (v *configString) setFromEnv(variableName string) {
	if value, ok := os.LookupEnv(variableName); ok && value != "" {
		v.set(value)
	}
}

func (v *configString) setIfEmpty(value string) {
	if *v == "" {
		v.set(value)
	}
}

func (v *configString) string() string {
	return string(*v)
}

func (c *Config) applyDefaults() {
	base := float64(1024 * 1024 * 1024)
	c.DataPath.setIfEmpty("/var/lib/uploads")
	c.ScanTime.setIfEmpty("30s")
	c.ListenPort.setIfEmpty("8080")
	c.TTL.setIfEmpty("24h")
	c.BasePath.setIfEmpty("/")
	c.WebIndex.setIfEmpty("index.html")
	c.Scheme.setIfEmpty("https")
	if c.MaxUploadBytes == 0 {
		c.MaxUploadBytes = int64(2 * base)
	}
}

func newConfigFromFile(file string) (Config, error) {
	var conf Config
	configData, err := os.ReadFile(file)
	if err != nil {
		return conf, err
	}
	toml.Unmarshal(configData, &conf)

	return conf, nil
}

func (c *Config) applyFromEnv() {
	c.DataPath.setFromEnv("UPLOAD_DATAPATH")
	c.ScanTime.setFromEnv("UPLOAD_SCANTIME")
	c.ListenPort.setFromEnv("UPLOAD_LISTENPORT")
	c.TTL.setFromEnv("UPLOAD_TTL")
	c.BasePath.setFromEnv("UPLOAD_BASEPATH")
	c.WebIndex.setFromEnv("UPLOAD_WEBINDEX")
	c.Scheme.setFromEnv("UPLOAD_SCHEME")
	c.PresetAdminToken.setFromEnv("UPLOAD_PRESETADMINTOKEN")

	// need a little more safety around this one
	if os.Getenv("UPLOAD_MAXUPLOADBYTES") != "" {
		maxBytesInt, err := strconv.ParseInt(os.Getenv("UPLOAD_MAXUPLOADBYTES"), 10, 64)
		if err != nil {
			panic(err)
		}
		c.MaxUploadBytes = maxBytesInt
	}
}

func (c *Config) logOptions() {
	log.Printf("Using data directory: %s", c.DataPath)
	log.Printf("Using TTL of: %s", c.TTL)
	log.Printf("listening on: %s", c.ListenPort)
	log.Printf("using scheme: %s", c.Scheme)
	log.Printf("Max upload size: %d bytes", c.MaxUploadBytes)
}
