# A temporary upload server

## running

```bash
go build
./upload --config=/path/to/config.toml
```

or

```
podman run -d -v /path/to/config.toml:/etc/upload.toml:ro -v upload-data:/var/lib/upload -p 8080:8080 codeberg.org/sparrow/upload:latest
```

## example config

It's not necessary to set all of these options, the example below lists the defaults.

```toml
dataPath = '/var/lib/upload'
ttl = '24h'
scanTime = '30s'
listenPort = '8080'
basePath = '/upload'
```

at a minimum you'll want to set `dataPath`, `port`

Time units for TTL and scanTime are taken from go's `time.ParseDuration()`
Valid time units are "ns", "us" (or "µs"), "ms", "s", "m", "h".

- `dataPath` is where you want the data to be stored
- `ttl` is how long you want uploaded files to live before being deleted
- `scanTime` is how often you want to check for expired files
- `listenPort` is the port you want the application to listen on
- `basePath` is the base path to this server (i.e /upload)
- `scheme` is the target scheme for redirects if you want to explicitly set this. defaults to https
- `presetAdminToken` allows you to use a predefined string as the initial admin token instead of the default behaviour of randomly generating the initial admin token.
- `maxUploadBytes` is the upload limit in bytes.

It is also possible to set config options through environment variables:

- UPLOAD_DATAPATH
- UPLOAD_TTL
- UPLOAD_SCANTIME
- UPLOAD_LISTENPORT
- UPLOAD_BASEPATH
- UPLOAD_SCHEME
- UPLOAD_PRESETADMINTOKEN
- UPLOAD_MAXUPLOADBYTES

## uploading

`curl -H"Authentication: <token>" http://localhost:8080 --form file=@somefile.png`

You have the option when uploading a file to make it "private", this file can only be downloaded by another authenticated user.

`curl -H"Authentication: <token>" http://localhost:8080 --form file=@somefile.png --form private=true`

And make a file available only for a single download:

`curl -H"Authentication: <token>" http://localhost:8080 --form file=@somefile.png --form single=true`

or both

`curl -H"Authentication: <token>" http://localhost:8080 --form file=@somefile.png --form private=true --form single=true`


## auth tokens

On first start, the application will output it's first admin token in the logs. keep this somewhere safe.

You can use the initial admin tokens to create new user and admin tokens.

Authentication can be done one of three ways (for compatibility):

1) the header: `Authentication: <password>`
2) the header: `Authorization: bearer <password>`
3) basic auth

## admin actions

holders of admin keys can interact with the server with the following methods:

- `GET <basepath>/admin/user/` lists all active token names, their timestamp, and a boolean for if they are admin.
- `GET <basepath>/admin/files/` lists all files with their deletion date and a link to the file
- `PUT <basepath>/admin/user/<name>` creates a new token with the name specified, returns the token's authentication key
- `DELETE <basepath>/admin/user/<name>` revokes a token
- `DELETE <basepath>/file.ext` manually removes a file
